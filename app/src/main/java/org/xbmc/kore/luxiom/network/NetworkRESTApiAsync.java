package org.xbmc.kore.luxiom.network;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;
import org.xbmc.kore.luxiom.interfaces.NetworkRESTApiAsyncCallback;
import org.xbmc.kore.luxiom.interfaces.NetworkRESTApiAsyncUIUpdateCallback;
import org.xbmc.kore.luxiom.model.RequestModelWrapper;
import org.xbmc.kore.utils.LogUtils;
import org.xbmc.kore.utils.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import static org.xbmc.kore.luxiom.Constants.DIR_KODI_JSON;

/**
 * Created by OMIPLEKEVIN on Sep 26, 026.
 */

public class NetworkRESTApiAsync extends AsyncTask<Void, Void, RequestModelWrapper> {
    private static final int REQUEST_TIMEOUT = 5 * 1000;
    private final String TAG = LogUtils.makeLogTag(NetworkRESTApiAsync.class);

    String sourceUrl;
    boolean enableCacheConfig;
    String parentDir;
    String fileName;
    String method;
    String tag;
    View targetUpdateView;
    TreeMap<String, String> map;
    NetworkRESTApiAsyncCallback callback;
    NetworkRESTApiAsyncUIUpdateCallback uiCallback;

    public NetworkRESTApiAsync(String sourceUrl, boolean enableCacheConfig, String parentDir, String fileName, String method, TreeMap<String, String> map) {
        this.sourceUrl = sourceUrl;
        this.enableCacheConfig = enableCacheConfig;
        this.parentDir = parentDir;
        this.fileName = fileName;
        this.method = method;
        this.map = map;
    }

    public void setNetworkRESTApiAsyncCallback(NetworkRESTApiAsyncCallback callback, String tag) {
        this.tag = tag;
        this.callback = callback;
    }

    public void setNetworkRESTApiAsyncUIUpdateCallback(View targetUpdateView, NetworkRESTApiAsyncUIUpdateCallback callback){
        this.targetUpdateView = targetUpdateView;
        this.uiCallback = callback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected RequestModelWrapper doInBackground(Void... params) {
        Log.v(TAG, "-\nSource:" + sourceUrl + "\nEnableCacheConfig:" + enableCacheConfig + "\nFilename:" + fileName + "\nMethod:" + method + "\nMap:" + map);
        StringBuilder responseString = new StringBuilder();
        try {
            Log.v("urlSpeed", "Start");
            //throws MalformedURLException
            URL urlSource = new URL(sourceUrl);
            //throws IOException
            HttpURLConnection httpUrlConnection = (HttpURLConnection) urlSource.openConnection();
            httpUrlConnection.setRequestMethod(method.toUpperCase(Locale.getDefault()));
            httpUrlConnection.setConnectTimeout(REQUEST_TIMEOUT);

            //lets check first if we are not getting a blind error
            if (httpUrlConnection.getHeaderField("Status") != null && !httpUrlConnection.getHeaderField("Status").isEmpty()) {
                Log.e(TAG, "fault in connection...");
                RequestModelWrapper response = new RequestModelWrapper();
                response.requestCode = RequestModelWrapper.REQUEST_BAD;
                response.requestResult = httpUrlConnection.getHeaderField("Status") + " @ " + sourceUrl;
                return response;
            } else {
                Log.i(TAG, "lets proceed...");
                if (method.equals("POST")) {
                    StringBuilder urlParams = new StringBuilder();
                    for (Map.Entry<String, String> entry : map.entrySet()) {
                        urlParams.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
                    }
                    httpUrlConnection.setDoOutput(true);
                    httpUrlConnection.setInstanceFollowRedirects(false);
                    httpUrlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    httpUrlConnection.setRequestProperty("charset", "utf-8");
                    OutputStreamWriter writer = new OutputStreamWriter(httpUrlConnection.getOutputStream());
                    writer.write(urlParams.substring(0, urlParams.length() - 1));
                    writer.flush();
                }

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpUrlConnection.getInputStream()));
                Log.v("urlSize", sourceUrl + "\n" + httpUrlConnection.getContentLength());

                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    responseString.append(line);
                }

                httpUrlConnection.getInputStream().close();
                httpUrlConnection.disconnect();

                //this means that we are not doing UI update back to the Devices because we are not passing any value from the device
                if (targetUpdateView == null) {
                    RequestModelWrapper parseCheckResponse = doJsonParseCheck(responseString.toString());
                    if (parseCheckResponse.requestCode == RequestModelWrapper.REQUEST_OK) {
                        if (enableCacheConfig && fileName != null) {
                            Utils.writeToCacheFile(responseString.toString(), DIR_KODI_JSON + "/" + fileName, false);
                        }
                    } else {
                        return parseCheckResponse;
                    }
                } else {
                    //so we have a view to update, now, lets check if the reponse is OK or not
                    RequestModelWrapper uiUpdateResponse = new RequestModelWrapper();
                    uiUpdateResponse.requestCode = RequestModelWrapper.REQUEST_OK;
                    uiUpdateResponse.requestResult = responseString.toString();
                    return uiUpdateResponse;
                }
            }
        } catch (MalformedURLException malUrle) {
            Log.e(getClass().getSimpleName(), "MalformedURLException");
            malUrle.printStackTrace();
            RequestModelWrapper exceptionResponse = new RequestModelWrapper();
            exceptionResponse.requestResult = malUrle.getMessage();
            exceptionResponse.requestCode = RequestModelWrapper.REQUEST_BAD;
            return exceptionResponse;
        } catch (IOException ioe) {
            Log.e(getClass().getSimpleName(), "IOException");
            ioe.printStackTrace();
            RequestModelWrapper exceptionResponse = new RequestModelWrapper();
            exceptionResponse.requestResult = ioe.getLocalizedMessage();
            exceptionResponse.requestCode = RequestModelWrapper.REQUEST_BAD;
            return exceptionResponse;
        } catch (NullPointerException nullE) {
            Log.e(getClass().getSimpleName(), "NullPointerException");
            nullE.printStackTrace();
            RequestModelWrapper exceptionResponse = new RequestModelWrapper();
            exceptionResponse.requestResult = nullE.getMessage();
            exceptionResponse.requestCode = RequestModelWrapper.REQUEST_BAD;
            return exceptionResponse;
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Exception");
            e.printStackTrace();
            RequestModelWrapper exceptionResponse = new RequestModelWrapper();
            exceptionResponse.requestResult = e.getLocalizedMessage();
            exceptionResponse.requestCode = RequestModelWrapper.REQUEST_BAD;
            return exceptionResponse;
        }

        RequestModelWrapper goodResponse = new RequestModelWrapper();
        //instead of the json string to return, we'll pass the filename because we had it updated correctly
        goodResponse.requestResult = fileName;
        goodResponse.requestCode = RequestModelWrapper.REQUEST_OK;

        return goodResponse;
    }

    @Override
    protected void onPostExecute(RequestModelWrapper s) {
        Log.d(TAG, s.toString());
        //this means that we have a UI to update
        if (this.targetUpdateView != null) {
            uiCallback.onNetworkRequestUIUpdateResult(this.targetUpdateView, s.requestCode == 1, s);
        } else if (callback != null) {
            callback.onNetworkRequestResult(s, (this.tag != null ? this.tag : ""));
        }
        super.onPostExecute(s);
    }

    private RequestModelWrapper doJsonParseCheck(String jsonContent){

        RequestModelWrapper requestModelWrapper;
        try {
            // TODO: Sep 18, 018 YOU NEED TO DO MORE RIGID JSON VALIDATION HERE!!!!!
            JSONObject jsonObject = new JSONObject(jsonContent);
            JsonParser parser = new JsonParser();
            parser.parse(jsonContent);
            requestModelWrapper = new RequestModelWrapper();
            requestModelWrapper.requestCode = RequestModelWrapper.REQUEST_OK;
            return requestModelWrapper;
        } catch (JSONException jsonException) {
            jsonException.printStackTrace();
            requestModelWrapper = new RequestModelWrapper();
            requestModelWrapper.requestCode = RequestModelWrapper.REQUEST_BAD;
            requestModelWrapper.requestResult = jsonException.getLocalizedMessage();
            return requestModelWrapper;
        } catch (JsonSyntaxException syntaxException) {
            syntaxException.printStackTrace();
            requestModelWrapper = new RequestModelWrapper();
            requestModelWrapper.requestCode = RequestModelWrapper.REQUEST_BAD;
            requestModelWrapper.requestResult = syntaxException.getLocalizedMessage();
            return requestModelWrapper;
        }
    }
}
