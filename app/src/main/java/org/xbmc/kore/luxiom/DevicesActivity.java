package org.xbmc.kore.luxiom;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.database.DatabaseUtils;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xbmc.kore.R;
import org.xbmc.kore.luxiom.interfaces.NetworkRESTApiAsyncUIUpdateCallback;
import org.xbmc.kore.luxiom.model.DeviceData;
import org.xbmc.kore.luxiom.model.DeviceModel;
import org.xbmc.kore.luxiom.model.RequestModelWrapper;
import org.xbmc.kore.luxiom.model.RoomModel;
import org.xbmc.kore.luxiom.network.NetworkRESTApiAsync;
import org.xbmc.kore.ui.BaseActivity;
import org.xbmc.kore.ui.NavigationDrawerFragment;
import org.xbmc.kore.utils.LogUtils;
import org.xbmc.kore.utils.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

import static org.xbmc.kore.luxiom.Constants.DEVICES_JSON_FILENAME;
import static org.xbmc.kore.luxiom.Constants.DIR_KODI_JSON;
import static org.xbmc.kore.luxiom.Constants.ROOMS_JSON_FILENAME;

public class DevicesActivity extends BaseActivity implements MqttCallback {
    private static final String TAG = LogUtils.makeLogTag(DevicesActivity.class);

    public static final String ERR_NO_DEVICES_JSON = "Please update, no devices.json";
    public static final String ERR_NO_ROOMS_JSON = "Please update, no rooms.json";

    private static final String CONTROLLER = "/data_request?id=lu_action&serviceId=";
    private static String SERVICE_ID_SWITCH = "urn:upnp-org:serviceId:SwitchPower1";
    private static String SERVICE_ID_DIMMABLE = "urn:upnp-org:serviceId:Dimming1";
    private static String ACTION;

    private static final String MQTT_TOPIC_TREE_DEVICES = "/binarybean/vera/status/devices";

    /**
     * we need this because we need to get the friendly name specified on the bluetooth name.
     * if ever we need to
     */
    private static final int PERMISSIONS_REQUEST_BLUETOOTH = 101;

    private NavigationDrawerFragment navigationDrawerFragment;
    private List<DeviceModel> devices;
    private List<RoomModel> rooms;
    private MqttAndroidClient mqttAndroidClient;
    private LuxiomPrefsManager luxiomPrefsManager;
    private ExpandableListViewAdapter expandableListViewAdapter;
    private HashMap<String, LinkedHashMap<Integer, DeviceData>> mapping;
    private boolean hasStartedMqtt = false;

    List<DeviceData> deviceDataList = null;

    private String PORT = "";
    private String HOST = "";

    private Toast toast;

    @InjectView(R.id.default_toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_devices);
        ButterKnife.inject(this);

        // Set up the drawer.
        navigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager()
                .findFragmentById(R.id.navigation_drawer);
        navigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));
        setupActionBar();

        toast = Toast.makeText(this, null, Toast.LENGTH_SHORT);
        // If we should start playing something

//        // Setup system bars and content padding
//        setupSystemBarsColors();
//        // Set the padding of views.
//        // Only set top and right, to allow bottom to overlap in each fragment
//        UIUtils.setPaddingForSystemBars(this, viewPager, true, true, false);
//        UIUtils.setPaddingForSystemBars(this, pageIndicator, true, true, false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        luxiomPrefsManager = new LuxiomPrefsManager(this);

        PORT = luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.ZWAVE_CONF_PORT, "3480");
        HOST = luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.ZWAVE_CONF_HOST, "");

        Log.i(TAG, PreferenceManager.getDefaultSharedPreferences(this).getAll().toString());
        checkBluetoothPermissionAndUpdate();
        updateRooms();
        updateDevices();
        getRoomDeviceMapping();
        if (!hasStartedMqtt && setupExpandableListView()) {
            hasStartedMqtt = true;
            startMqttService(devices);
        } else {
            Log.i(getClass().getSimpleName(), "MQTT has already started");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disconnectMqtt();
    }

    private boolean checkBluetoothPermissionAndUpdate() {
        int hasWritePermission = ContextCompat.checkSelfPermission(DevicesActivity.this, Manifest.permission.BLUETOOTH);
        // Here, thisActivity is the current activity
        if (hasWritePermission != PackageManager.PERMISSION_GRANTED) {

            if (!ActivityCompat.shouldShowRequestPermissionRationale(DevicesActivity.this, Manifest.permission.BLUETOOTH)) {
                showMessageOKCancel("You need to allow access to Bluetooth",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(DevicesActivity.this, new String[]{Manifest.permission.BLUETOOTH},
                                        PERMISSIONS_REQUEST_BLUETOOTH);
                            }
                        });
                return false;
            }
            ActivityCompat.requestPermissions(DevicesActivity.this, new String[]{Manifest.permission.BLUETOOTH},
                    PERMISSIONS_REQUEST_BLUETOOTH);
            return false;
        }
        return true;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(DevicesActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!navigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen if the drawer is not showing.
            // Otherwise, let the drawer decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.remote, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here.

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        hasStartedMqtt = false;
        try {
            if (mapping != null) {
                for (DeviceModel deviceItem : devices) {
                    unsubscribeMqtt(MQTT_TOPIC_TREE_DEVICES + "/" + deviceItem.id);
                }
//                disconnectMqtt();
            }
        } catch (Exception e) {
//            disconnectMqtt();
            e.printStackTrace();
        }
    }

    private void startMqttService(final List<DeviceModel> devices) {
        BluetoothAdapter myDevice = BluetoothAdapter.getDefaultAdapter();
        String deviceName = "";
        if (myDevice == null) {
            deviceName = Build.MODEL;
        } else {
            deviceName = myDevice.getName();
        }


        String macAddress = "";
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
            WifiInfo wInfo = wifiManager.getConnectionInfo();
            macAddress = wInfo.getMacAddress();
        } else {
            try {
                List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
                for (NetworkInterface nif : all) {
                    if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                    byte[] macBytes = nif.getHardwareAddress();
                    if (macBytes == null) {
                        macAddress = "";
                        break;
                    }

                    StringBuilder res1 = new StringBuilder();
                    for (byte b : macBytes) {
                        res1.append(String.format("%02X:", b));
                    }

                    if (res1.length() > 0) {
                        res1.deleteCharAt(res1.length() - 1);
                    }
                    macAddress = res1.toString();
                    break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        deviceName = deviceName.replace("\'", "");
        String clientId = "Luxiom-" + deviceName;
//        String clientId = "Luxiom-" + (!macAddress.equals("") ? macAddress:System.currentTimeMillis());

        Log.i(TAG,
                "================================================\n" +
                        "deviceName: " + deviceName + ", client: " + clientId +
                        "\n================================================");

        Log.i(TAG, "tcp://" + luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.MQTT_CONF_HOST, "")
                + ":" + luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.MQTT_CONF_PORT, ""));

        mqttAndroidClient =
                new MqttAndroidClient(this.getApplicationContext(),
                        "tcp://" + luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.MQTT_CONF_HOST, "")
                                + ":" + luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.MQTT_CONF_PORT, ""),
                        clientId);

        try {
            mqttAndroidClient.setCallback(this);
            IMqttToken token = mqttAndroidClient.connect();
            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // We are connected
                    Log.d(TAG, "onSuccess");
                    for (DeviceModel deviceItem : devices) {
                        subscribeMqtt(MQTT_TOPIC_TREE_DEVICES + "/" + deviceItem.id);
                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    // Something went wrong e.g. connection timeout or firewall problems
                    Toast.makeText(DevicesActivity.this, "Failed to start MQTT Service", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "startMqttService on Failure " + exception);
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    private void subscribeMqtt(String topic) {
        final int qos = 1;
        try {
            IMqttToken subToken = mqttAndroidClient.subscribe(topic, qos);
            final String fTopic = topic;
            subToken.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // The message was published
                    Log.i(TAG, Arrays.toString(asyncActionToken.getTopics()) + " - " + asyncActionToken.getClient().getClientId() + " - " + asyncActionToken.getClient().getServerURI());
                    Log.e(TAG, "onSuccess");
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken,
                                      Throwable exception) {
                    Toast.makeText(DevicesActivity.this, "MQTT Failed to subscribe @ " + fTopic, Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "onFailure");
                    // The subscription could not be performed, maybe the user was not
                    // authorized to subscribe on the specified topic e.g. using wildcards

                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    private void unsubscribeMqtt(final String topic) {
        try {
            IMqttToken unsubToken = mqttAndroidClient.unsubscribe(topic);
            unsubToken.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Log.i(TAG, "onSuccess unsubscribeMqtt on" + topic);
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken,
                                      Throwable exception) {
                    toast.setText("MQTT Failed to unsubscribe");
                    toast.show();
                    // some error occurred, this is very unlikely as even if the client
                    // did not had a subscription to the topic the unsubscribe action
                    // will be successfully
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void disconnectMqtt() {
        try {
            IMqttToken disconToken = mqttAndroidClient.disconnect();
            disconToken.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Log.i(TAG, "onSuccess disconnectMqtt");
                    // we are now successfully disconnected
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken,
                                      Throwable exception) {
                    // something went wrong, but probably we are disconnected anyway
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void connectionLost(Throwable cause) {
        try {
            Log.e(TAG, "connection lost - " + cause.getMessage());
            Toast.makeText(this, cause.getMessage(), Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        try {
            Log.d(TAG, "deliveryComplete - " + token.getClient().getClientId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupActionBar() {
        toolbar.setTitle("Devices");
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null) return;
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void updateRooms() {

        File roomFiles = new File(DIR_KODI_JSON + "/" + ROOMS_JSON_FILENAME);
        if (!roomFiles.isFile()) {
//            getRoomsJSON();
        } else {
            if (rooms != null) {
                rooms.clear();
            } else {
                rooms = new ArrayList<>();
            }
            try {
                JSONObject roomsRawJson = new JSONObject(Utils.readCacheFile(DIR_KODI_JSON + "/" + ROOMS_JSON_FILENAME));
                JSONArray roomsArray = roomsRawJson.getJSONArray("array");
                for (int i = 0; i < roomsArray.length(); i++) {
                    try {
                        JSONObject obj = roomsArray.getJSONObject(i);
                        Log.e(TAG, obj.toString());
                        int[] devicesList = new int[obj.getJSONArray("DIR").length()];
                        for (int x = 0; x < obj.getJSONArray("DIR").length(); x++) {
                            devicesList[x] = obj.getJSONArray("DIR").getInt(x);
                        }
                        RoomModel entry = new RoomModel(obj.getString("Name"), obj.getInt("ID"), devicesList);
                        rooms.add(entry);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateDevices() {

        File devicesFiles = new File(DIR_KODI_JSON + "/" + DEVICES_JSON_FILENAME);

        if (!devicesFiles.isFile()) {
//            getDevicesJSON();
        } else {
            if (devices == null) {
                devices = new ArrayList<>();
            } else {
                devices.clear();
            }
            try {
                JSONObject devicesRawJson = new JSONObject(Utils.readCacheFile(DIR_KODI_JSON + "/" + DEVICES_JSON_FILENAME));
                JSONArray devicesArray = devicesRawJson.getJSONArray("array");
                for (int i = 0; i < devicesArray.length(); i++) {
                    JSONObject obj = devicesArray.getJSONObject(i);
                    DeviceModel entry = new DeviceModel(obj.getInt("ID"), obj.getString("TYPE"), obj.getString("NAME"));
                    devices.add(entry);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean setupExpandableListView() {
        if (mapping != null) {
            ExpandableListView expandableListView = (ExpandableListView) findViewById(R.id.roomsExpandableListView);
            expandableListViewAdapter = new ExpandableListViewAdapter(this, getListHeaders(), mapping);
            Log.e(getClass().getSimpleName(), "mapping in expandableList is " + mapping.toString());
            expandableListView.setAdapter(expandableListViewAdapter);
            return true;
        } else {
            Toast.makeText(this, "Unable to render device, please update cache", Toast.LENGTH_LONG).show();
            Log.e(getClass().getSimpleName(), "mapping is null! WTH!");
            return false;
        }
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) {
        Log.e(TAG, topic + " - " + message);
//        Log.e(TAG, mapping.toString());
        if (!topic.contains("devices")) {
            Log.w(getClass().getSimpleName(), "getting " + topic + ", out of my subscription");
        } else {
            try {
                boolean hasUpdated = false;
                JSONObject jsonMessage = new JSONObject(message.toString());
                Log.e("messageArrived", jsonMessage.toString() + " ");
                for (Map.Entry<String, LinkedHashMap<Integer, DeviceData>> entry : mapping.entrySet()) {
                    for (Map.Entry<Integer, DeviceData> innerEntry : entry.getValue().entrySet()) {
                        Log.w(TAG, String.valueOf(jsonMessage.getInt("id")) + " " + String.valueOf(innerEntry.getKey()) + " | " + topic.substring(topic.lastIndexOf('/') + 1) + " == " + jsonMessage.getString("id"));
                        if (String.valueOf(jsonMessage.getInt("id")).equals(String.valueOf(innerEntry.getKey()))
                                && (topic.substring(topic.lastIndexOf('/') + 1).equals(jsonMessage.getString("id")))) {

                            Log.v(TAG, "switching " + getDeviceTypeID(jsonMessage.getInt("id")));

                            switch (getDeviceTypeID(jsonMessage.getInt("id"))) {
                                case "slide_bar":
                                    Log.e(getClass().getSimpleName(), "Dimmer here... with json: " + jsonMessage.toString());
                                    Log.e(getClass().getSimpleName(), entry.getValue().get(jsonMessage.getInt("id")).slideValue + " set to " + Integer.parseInt(jsonMessage.getString("level")));
                                    //                                entry.getValue().get(jsonMessage.getInt("id")).deviceType = jsonMessage.getString("name");
                                    entry.getValue().get(jsonMessage.getInt("id")).slideValue = Integer.parseInt(jsonMessage.getString("level"));
                                    expandableListViewAdapter.notifyDataSetChanged();
                                    hasUpdated = true;
                                    break;
                                case "button":
                                    hasUpdated = true;
                                    break;
                                case "dimmable_switch":
                                    Log.e(TAG, "parse: " + jsonMessage.getInt("id") + " " + getDeviceTypeID(jsonMessage.getInt("id")));
                                    if (entry.getValue().get(jsonMessage.getInt("id")) == null) {
                                        Log.e(TAG, "deviceData is null");
                                    }
                                    if (Integer.parseInt(jsonMessage.getString("status")) == 1) {
                                        entry.getValue().get(jsonMessage.getInt("id")).dimValue = jsonMessage.getInt("level");
                                    } else {
                                        entry.getValue().get(jsonMessage.getInt("id")).dimValue = 0;
                                    }
                                    expandableListViewAdapter.notifyDataSetChanged();
                                    hasUpdated = true;
                                    break;
                                case "dimmer":
                                    if ((jsonMessage.getString("comment").contains("SUCCESS")  || jsonMessage.getString("comment").equals(""))
                                            && String.valueOf(innerEntry.getKey()).equals(String.valueOf(jsonMessage.getInt("id")))) {
                                        Log.e(TAG, "parse: " + jsonMessage.getInt("id") + " " + getDeviceTypeID(jsonMessage.getInt("id")));
                                        if (entry.getValue().get(jsonMessage.getInt("id")) == null) {
                                            Log.e(TAG, "deviceData is null");
                                        }
                                        entry.getValue().get(jsonMessage.getInt("id")).dimValue = jsonMessage.getInt("level");
                                        expandableListViewAdapter.notifyDataSetChanged();
                                    }
                                    hasUpdated = true;
                                    break;
                                case "binary_switch":
                                    if (jsonMessage.getString("comment").contains("SUCCESS") || jsonMessage.getString("comment").equals("")) {
                                        Log.e(TAG, "parse: " + jsonMessage.getInt("id") + " " + getDeviceTypeID(jsonMessage.getInt("id")));
                                        if (entry.getValue().get(jsonMessage.getInt("id")) == null) {
                                            Log.e(TAG, "deviceData is null");
                                        }
                                        entry.getValue().get(jsonMessage.getInt("id")).binarySwitchState = (Integer.parseInt(jsonMessage.getString("status")) == 1);
                                        expandableListViewAdapter.notifyDataSetChanged();
                                    }
                                    hasUpdated = true;
                                    break;
                                case "blind":
                                    hasUpdated = true;
                                    break;
                                case "door":
                                    Log.e(TAG, "parse: " + jsonMessage.getInt("category") + " " + getDeviceTypeID(jsonMessage.getInt("category")));
                                    if (entry.getValue().get(jsonMessage.getInt("category")) == null) {
                                        Log.e(TAG, "deviceData is null");
                                    }
                                    entry.getValue().get(jsonMessage.getInt("category")).doorSwitchState = jsonMessage.getBoolean("value");
                                    expandableListViewAdapter.notifyDataSetChanged();
                                    hasUpdated = true;
                                    break;
                                case "rgbw":
                                    if (jsonMessage.getString("comment").contains("SUCCESS") || jsonMessage.getString("comment").equals("")) {
                                        JSONArray valueArr = jsonMessage.getJSONArray("value");
                                        Log.e(TAG, "parse: " + jsonMessage.getInt("category") + " " + getDeviceTypeID(jsonMessage.getInt("category")));
                                        if (entry.getValue().get(jsonMessage.getInt("category")) == null) {
                                            Log.e(TAG, "deviceData is null");
                                        }
                                        entry.getValue().get(jsonMessage.getInt("category")).redvalue = valueArr.getInt(0);
                                        entry.getValue().get(jsonMessage.getInt("category")).greenValue = valueArr.getInt(1);
                                        entry.getValue().get(jsonMessage.getInt("category")).blueValue = valueArr.getInt(2);
                                        entry.getValue().get(jsonMessage.getInt("category")).whiteValue = valueArr.getInt(3);
                                        expandableListViewAdapter.notifyDataSetChanged();
                                    }
                                    hasUpdated = true;
                                    break;
                                default:
                                    Toast.makeText(this, "Unknown Device Type", Toast.LENGTH_LONG).show();
                                    //pass block <code>if(!hasUpdated)</code> condition
                                    hasUpdated = true;
                                    break;
                            }
                            break;
                        }
                    }
                }

                if (!hasUpdated) {
                    Log.e(TAG, "Mismatched ID(" + jsonMessage.getInt("id") + ") from " + topic + " ======================================== ");
                    Toast.makeText(this, "Mismatched ID(" + jsonMessage.getInt("id") + ") from " + topic, Toast.LENGTH_LONG).show();
                }

            } catch (JSONException jsonEx) {
                Toast.makeText(this, "Invalid message from " + topic + " (" + jsonEx.getMessage() + ")", Toast.LENGTH_LONG).show();
                jsonEx.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class ExpandableListViewAdapter extends BaseExpandableListAdapter {

        private Context _context;
        private List<String> _listDataHeader; // header titles
        private List<String> _listDataIdHeader;
        // child data in format of header title, child title
        private HashMap<String, LinkedHashMap<Integer, DeviceData>> _listDataChild;

        public ExpandableListViewAdapter(Context context, List<String> listDataHeader,
                                         HashMap<String, LinkedHashMap<Integer, DeviceData>> listChildData) {
            this._context = context;
            //lets map out again the room names
            _listDataHeader = new ArrayList<>();
            _listDataIdHeader = listDataHeader;
            for (int i = 0; i < listDataHeader.size(); i++) {
                for (RoomModel roomItem : rooms) {
                    if (listDataHeader.get(i).equals(String.valueOf(roomItem.roomID))) {
                        _listDataHeader.add(roomItem.name);
                    }
                }
            }
            this._listDataChild = listChildData;
        }

        @Override
        public int getGroupCount() {
            return _listDataIdHeader.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return _listDataChild.get(_listDataIdHeader.get(groupPosition)).size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return _listDataHeader.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return _listDataChild.get(_listDataIdHeader.get(groupPosition));
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        class ViewHolder {
            TextView deviceName;
            View childView;
            String deviceType;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = LayoutInflater.from(this._context).inflate(R.layout.expandable_list_view_devices_separator, null);
                viewHolder.deviceName = (TextView) convertView;
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.deviceName.setText((String) getGroup(groupPosition));
            viewHolder.deviceType = viewHolder.deviceName.getText().toString();
            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            //don't recycle views for now
            Log.e(TAG, "GP: " + groupPosition + " CP:" + childPosition);
            int childDeviceId;
            LayoutInflater inflater = LayoutInflater.from(this._context);
            convertView = inflater.inflate(R.layout.expandable_list_view_devices, null);
//            childDeviceId = _listDataChild.get(_listDataHeader.get(groupPosition))[childPosition];
            DeviceData childDeviceData = (new ArrayList<>(_listDataChild.get(_listDataIdHeader.get(groupPosition)).values())).get(childPosition);
            childDeviceId = childDeviceData.deviceID;
            ((LinearLayout) convertView).addView(getChildDevicesView(Integer.parseInt(_listDataIdHeader.get(groupPosition)), childDeviceId, inflater, childDeviceData));

            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return false;
        }

        private View getChildDevicesView(int groupPosition, int deviceListId, LayoutInflater inflater, final DeviceData deviceData) {
            final String groupPos = String.valueOf(groupPosition);
            Log.e("CHILD-ADD", "getChildDevicesView - " + deviceListId);
            LinearLayout linearLayout = new LinearLayout(this._context);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            linearLayout.setLayoutParams(params);
            linearLayout.setOrientation(LinearLayout.VERTICAL);


            View contentControlView = null;
            final DeviceData devData = deviceData;

            TextView deviceLabel = (TextView) inflater.inflate(R.layout.expandable_list_view_devices_section, null);
            deviceLabel.setText(getDeviceName(deviceListId));
            deviceLabel.invalidate();

            Log.e(TAG, "looking for: " + getDeviceTypeID(deviceListId));
            // TODO: Sep 26, 026 catch when mapping is out of sync <code>getDeviceTypeID(deviceListId)</code>
            switch (getDeviceTypeID(deviceListId)) {
                case "slide_bar":
                    Log.e(TAG, "getting dimmer view...");
                    View simpleSlideBar = inflater.inflate(R.layout.view_plain_slide_bar, null);
                    ((SeekBar) simpleSlideBar.findViewById(R.id.simpleSeekBar)).setMax(100);
                    ((SeekBar) simpleSlideBar.findViewById(R.id.simpleSeekBar)).setProgress(devData.slideValue);
                    ((SeekBar) simpleSlideBar.findViewById(R.id.simpleSeekBar)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                            Log.i("SLIDE BAR", String.valueOf(progress));
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {
                            updateValue("slide_bar", seekBar, groupPos, String.valueOf(deviceData.deviceID), devData, String.valueOf(seekBar.getProgress()));
                        }
                    });
                    contentControlView = simpleSlideBar;
                    break;
                /*case "button":
                    View simpleBtnView = inflater.inflate(R.layout.view_plain_button, null);
                    ((Button) simpleBtnView.findViewById(R.id.simpleBtn)).setText("Simple Btn");
                    ((Button) simpleBtnView.findViewById(R.id.simpleBtn)).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            updateValue("button", v, groupPos, String.valueOf(deviceData.deviceID), devData, "some action...");
                        }
                    });
                    contentControlView = simpleBtnView;
                    break;*/
                case "dimmable_switch":
                    //dimmer switch
                    View dimmableSwitch = inflater.inflate(R.layout.view_dimmable_switch, null);
                    ((SeekBar) dimmableSwitch.findViewById(R.id.dimmerSwitchSeekbar)).setProgress(devData.dimValue);
                    ((SeekBar) dimmableSwitch.findViewById(R.id.dimmerSwitchSeekbar)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                            Log.i("DIMMER", String.valueOf(progress));
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {
                            updateValue("dimmable_switch", seekBar, groupPos, String.valueOf(deviceData.deviceID), devData, String.valueOf(seekBar.getProgress()));
                        }
                    });
                    ((Button) dimmableSwitch.findViewById(R.id.dimmableSwitchOff)).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            updateValue("dimmable_btn_off", v, groupPos, String.valueOf(deviceData.deviceID), devData, String.valueOf("0"));
                        }
                    });
                    ((Button) dimmableSwitch.findViewById(R.id.dimmableSwitchOn)).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            updateValue("dimmable_btn_on", v, groupPos, String.valueOf(deviceData.deviceID), devData, String.valueOf("1"));
                        }
                    });
                    contentControlView = dimmableSwitch;
                    break;
                case "dimmer":
                    //dimmer
                    View dimmableView = inflater.inflate(R.layout.view_dimmable_slider, null);
                    ((SeekBar) dimmableView.findViewById(R.id.dimmerSeekbar)).setProgress(deviceData.dimValue);
                    ((SeekBar) dimmableView.findViewById(R.id.dimmerSeekbar)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                            Log.i("DIMMER", String.valueOf(progress));
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {
                            updateValue("dimmer", seekBar, groupPos, String.valueOf(deviceData.deviceID), devData, String.valueOf(seekBar.getProgress()));
                        }
                    });
                    contentControlView = dimmableView;
                    break;
                case "binary_switch":
                    //binary switch
                    View binarySwitch = inflater.inflate(R.layout.view_binary_switch, null);
                    ((TextView) binarySwitch.findViewById(R.id.binaryOn)).setText("ON");
                    ((TextView) binarySwitch.findViewById(R.id.binaryOff)).setText("OFF");
                    ((Switch) binarySwitch.findViewById(R.id.binarySwitch)).setChecked(deviceData.binarySwitchState);
                    ((Switch) binarySwitch.findViewById(R.id.binarySwitch)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            Log.i("BINARYSWITCH", String.valueOf(isChecked));
                            updateValue("binary_switch", buttonView, groupPos, String.valueOf(deviceData.deviceID), devData, (isChecked ? "1" : "0"));
                        }
                    });
                    contentControlView = binarySwitch;
                    break;
                case "blind":
                    //blind
                    View blindSlider = inflater.inflate(R.layout.view_blinds, null);
                    ((Button) blindSlider.findViewById(R.id.blindsOpenBtn)).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            updateValue("blind", v, groupPos, String.valueOf(deviceData.deviceID), devData, String.valueOf("1"));
                        }
                    });
                    ((Button) blindSlider.findViewById(R.id.blindsStopBtn)).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            updateValue("blind", v, groupPos, String.valueOf(deviceData.deviceID), devData, String.valueOf("0"));
                        }
                    });
                    ((Button) blindSlider.findViewById(R.id.blindsCloseBtn)).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            updateValue("blind", v, groupPos, String.valueOf(deviceData.deviceID), devData, String.valueOf("0"));
                        }
                    });
                    contentControlView = blindSlider;
                    break;
                case "door":
                    View doorSwitch = inflater.inflate(R.layout.view_door_switch, null);
                    ((Switch) doorSwitch.findViewById(R.id.doorSwitch)).setChecked(deviceData.doorSwitchState);
                    ((Switch) doorSwitch.findViewById(R.id.doorSwitch)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            Log.i("door", String.valueOf(isChecked));
                            updateValue("", buttonView, groupPos, String.valueOf(deviceData.deviceID), devData, String.valueOf(isChecked));
                        }
                    });
                    contentControlView = doorSwitch;
                    break;
                case "rgbw":
                    //rgbw
                    View rgbwView = inflater.inflate(R.layout.view_rgbw_slider, null);
                    final SeekBar redSeekBar = (SeekBar) rgbwView.findViewById(R.id.redSeekBar);
                    final SeekBar greenSeekBar = (SeekBar) rgbwView.findViewById(R.id.greenSeekBar);
                    final SeekBar blueSeekBar = (SeekBar) rgbwView.findViewById(R.id.blueSeekBar);
                    final SeekBar whiteSeekBar = (SeekBar) rgbwView.findViewById(R.id.whiteSeekBar);
                    redSeekBar.setProgress(deviceData.redvalue);
                    redSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                            Log.e("REDSEEKBAR", String.valueOf(progress));
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {
                            updateValue("rgbw", redSeekBar, groupPos, String.valueOf(deviceData.deviceID), devData, String.valueOf(seekBar.getProgress()),
                                    String.valueOf(greenSeekBar.getProgress()),
                                    String.valueOf(blueSeekBar.getProgress()),
                                    String.valueOf(whiteSeekBar.getProgress()));
                        }
                    });
                    greenSeekBar.setProgress(deviceData.greenValue);
                    greenSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                            Log.i("GREENSEEKBAR", String.valueOf(progress));
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {
                            updateValue("rgbw", greenSeekBar, groupPos, String.valueOf(deviceData.deviceID), devData, String.valueOf(redSeekBar.getProgress()),
                                    String.valueOf(seekBar.getProgress()),
                                    String.valueOf(blueSeekBar.getProgress()),
                                    String.valueOf(whiteSeekBar.getProgress()));
                        }
                    });
                    blueSeekBar.setProgress(deviceData.blueValue);
                    blueSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                            Log.d("BLUESEEKBAR", String.valueOf(progress));
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {
                            updateValue("rgbw", blueSeekBar, groupPos, String.valueOf(deviceData.deviceID), devData, String.valueOf(redSeekBar.getProgress()),
                                    String.valueOf(greenSeekBar.getProgress()),
                                    String.valueOf(seekBar.getProgress()),
                                    String.valueOf(whiteSeekBar.getProgress()));
                        }
                    });
                    whiteSeekBar.setProgress(deviceData.whiteValue);
                    whiteSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                            Log.v("WHITESEEKBAR", String.valueOf(progress));
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {
                            updateValue("rgbw", whiteSeekBar, groupPos, String.valueOf(deviceData.deviceID), devData, String.valueOf(redSeekBar.getProgress()),
                                    String.valueOf(greenSeekBar.getProgress()),
                                    String.valueOf(blueSeekBar.getProgress()),
                                    String.valueOf(seekBar.getProgress()));
                        }
                    });
                    contentControlView = rgbwView;
                    break;
            }

            if (contentControlView != null) {
                linearLayout.addView(deviceLabel);
                linearLayout.addView(contentControlView);
            }
            return linearLayout;
        }
    }

    private String getDeviceTypeID(int devId) {
        if (devices != null) {
            for (DeviceModel model : devices) {
                if (model.id == devId) {
                    Log.i(getClass().getSimpleName(), "getDeviceTypeID " + model.type);
                    return model.type.toLowerCase(Locale.getDefault());
                }
            }
        }
        return "";
    }

    Handler handler = new Handler();
    Runnable oldRunnable;

    private void updateValue(final String from, final View targetView, final String roomID, final String deviceID, final DeviceData deviceData, final String... newValues) {

        String service_id = "";
        String param = "";
        switch (from) {
            case "dimmable_switch":
            case "dimmer":
                service_id = SERVICE_ID_DIMMABLE;
                ACTION = "SetLoadLevelTarget";
                param = "newLoadlevelTarget=" + newValues[0];
                break;
            case "binary_switch":
                service_id = SERVICE_ID_SWITCH;
                ACTION = "SetTarget";
                param = "newTargetValue=" + newValues[0];
                break;
            case "dimmable_btn_off":
            case "dimmable_btn_on":
                service_id = SERVICE_ID_SWITCH;
                ACTION = "SetTarget";
                param = "newTargetValue=" + newValues[0];
                break;
            case "blind":
                service_id = SERVICE_ID_SWITCH;
                ACTION = "SetTarget";
                param = "newTargetValue=" + newValues[0];
                break;

        }

        if (oldRunnable != null) {
            Log.d("UPDATE-RM-RUNNABLE", "remove runnable...");
            handler.removeCallbacks(oldRunnable);
        } else {

        }
        final String f_service_id = service_id;
        final String f_param = param;

        Log.d("UPDATE-NEW-RUNNABLE", "create runnable...");
        Log.d(getClass().getSimpleName(), targetView + " " + roomID + " " + deviceID + " " + deviceData + " " + newValues.toString());
        oldRunnable = new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.i("UPDATE-API", "UPDATING...");
                        /*toast.setText(from + " updated to: " + Arrays.deepToString(newValues));
                        toast.show();*/
                        String host = luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.RPI_CONF_HOST, "");
                        String port = luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.RPI_CONF_PORT, "");

                        String appendParams = "?";
                        for (int i = 0; i < newValues.length; i++) {
                            appendParams = appendParams + "params" + (i + 1) + "=" + newValues[i];
                            //lets check if I have more params for the URL
                            if ((i + 1) < newValues.length - 1) {
                                appendParams = appendParams + "&";
                            }
                        }
                        String url = "http://" + HOST + ":" + PORT + CONTROLLER + f_service_id + "&action=" + ACTION + "&DeviceNum=" + deviceID + "&" + f_param + "&output_format=json";
                        Log.e(getClass().getSimpleName(), "append: " + appendParams);
                        NetworkRESTApiAsync networkRESTApiAsync = new NetworkRESTApiAsync(url,
                                false,
                                null,
                                null,
                                "GET",
                                null);
                        networkRESTApiAsync.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                    }
                });
            }
        };
        handler.postDelayed(oldRunnable, 300);
    }

    private String getDeviceName(int id) {
        if (devices != null) {
            for (DeviceModel device : devices) {
                if (device.id == id) {
                    return device.deviceName;
                }
            }
        }

        //common return - we dont have that ID | devices is null
        return "";
    }

    private List<String> getListHeaders() {
        ArrayList<java.lang.String> header = new ArrayList<>();
        if (rooms != null) {
            header = new ArrayList<>();
            for (RoomModel item : rooms) {
                header.add(String.valueOf(item.roomID));
            }
        }

        return header;
    }

    private HashMap<String, LinkedHashMap<Integer, DeviceData>> getRoomDeviceMapping() {
        if (rooms != null) {
            mapping = new HashMap<>();
            for (RoomModel item : rooms) {
                LinkedHashMap<Integer, DeviceData> deviceData = new LinkedHashMap<>();
                for (int dID : item.devices) {
                    DeviceData deviceEntryData = new DeviceData();
                    deviceEntryData.deviceID = dID;
                    deviceEntryData.deviceType = item.name;
                    deviceData.put(dID, deviceEntryData);
                }
                mapping.put(String.valueOf(item.roomID), deviceData);
            }
        }

        if (mapping != null) {
            Log.e(TAG, "getRoomDeviceMapping " + mapping.toString());
        } else {
            Log.e(getClass().getSimpleName(), "mapping is null! WTH!");
        }

        return mapping;
    }
}
