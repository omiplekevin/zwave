package org.xbmc.kore.luxiom.model;

/**
 * Created by OMIPLEKEVIN on Sep 24, 024.
 */

public class SceneModel {

    public SceneModel(String sceneName, int sceneID) {
        this.sceneName = sceneName;
        this.sceneID = sceneID;
    }

    public String sceneName;

    public int sceneID;

    public boolean isActive = false;

}
