package org.xbmc.kore.luxiom;

import android.os.Environment;

/**
 * Created by OMIPLEKEVIN on Sep 24, 024.
 */

public class Constants {

    public static final String DIR_KODI = Environment.getExternalStorageDirectory() + "/Kodi";
    public static final String DIR_KODI_JSON = DIR_KODI + "/json";
    public static final String ROOMS_JSON_FILENAME = "rooms.json";
    public static final String DEVICES_JSON_FILENAME = "devices.json";
    public static final String SCENES_JSON_FILENAME = "scenes.json";

    //UPDATE API
    public static final String API_UPDATE = "iscientTA/controller/";

}
