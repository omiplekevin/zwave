package org.xbmc.kore.luxiom.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by OMIPLEKEVIN on Sep 15, 015.
 */

public class DeviceModel {

    @SerializedName("ID")
    public int id;

    @SerializedName("TYPE")
    public String type;

    @SerializedName("name")
    public String deviceName;

    public DeviceModel(int id, String type, String deviceName) {
        this.id = id;
        this.type = type;
        this.deviceName = deviceName;
    }

}
