package org.xbmc.kore.luxiom.model;

/**
 * Created by OMIPLEKEVIN on Sep 25, 025.
 */

public class KodiDeviceModel {

    public KodiDeviceModel(String kodiName, String kodiAddress, String kodiType) {
        this.kodiName = kodiName;
        this.kodiAddress = kodiAddress;
        this.kodiType = kodiType;
    }

    public String kodiName;

    public String kodiAddress;

    public String kodiType;
}
