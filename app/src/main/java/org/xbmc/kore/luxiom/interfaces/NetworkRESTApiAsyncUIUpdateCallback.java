package org.xbmc.kore.luxiom.interfaces;

import android.view.View;

import org.xbmc.kore.luxiom.model.RequestModelWrapper;

/**
 * Created by OMIPLEKEVIN on Sep 26, 026.
 */

public interface NetworkRESTApiAsyncUIUpdateCallback {

    void onNetworkRequestUIUpdateResult(View view, boolean isSuccess, RequestModelWrapper content);

}
