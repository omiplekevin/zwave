package org.xbmc.kore.luxiom.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by OMIPLEKEVIN on Sep 15, 015.
 */

public class RoomModel {

    @SerializedName("Name")
    public String name;

    @SerializedName("ID")
    public int roomID;

    @SerializedName("DIR")
    public int[] devices;

    public RoomModel(String name, int roomID, int[] devices) {
        this.name = name;
        this.roomID = roomID;
        this.devices = devices;
    }
}
