package org.xbmc.kore.luxiom.model;

/**
 * Created by OMIPLEKEVIN on Sep 22, 022.
 */

public class DeviceData {

    public String deviceType;
    public int deviceID;
    public int slideValue;
    public boolean buttonState;
    public int dimValue;
    public int rememberedValue;
    public boolean binarySwitchState;
    public int blindDropLevel;
    public boolean doorSwitchState;
    public int redvalue = 255;
    public int greenValue = 192;
    public int blueValue = 127;
    public int whiteValue = 64;

    @Override
    public String toString() {
        return
            "deviceType " + deviceType + " " +
            "deviceID " + deviceID + " " +
            "slideValue " + slideValue + " " +
            "buttonState " + buttonState + " " +
            "dimValue " + dimValue + " " +
            "rememberedValue " + rememberedValue + " " +
            "binarySwitchState " + binarySwitchState + " " +
            "blindDropLevel " + blindDropLevel + " " +
            "doorSwitchState " + doorSwitchState + " " +
            "redvalue " + redvalue  + " " +
            "greenValue " + greenValue  + " " +
            "blueValue " + blueValue  + " " +
            "whiteValue " + whiteValue;
    }
}
