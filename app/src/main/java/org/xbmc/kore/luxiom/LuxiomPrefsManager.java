package org.xbmc.kore.luxiom;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by OMIPLEKEVIN on Sep 21, 021.
 */

public class LuxiomPrefsManager {

    //RPI FIELDS
    public static final String RPI_CONF                 = "RPI";
    public static final String RPI_CONF_HOST            = "rpi_host";
    public static final String RPI_CONF_PORT            = "rpi_port";
    public static final String RPI_CONF_USERNAME        = "rpi_username";
    public static final String RPI_CONF_PASSWORD        = "rpi_password";

    //MQTT FIELDS
    public static final String MQTT_CONF                = "MQTT";
    public static final String MQTT_CONF_HOST           = "mqtt_host";
    public static final String MQTT_CONF_PORT           = "mqtt_port";
    public static final String MQTT_CONF_USERNAME       = "mqtt_username";
    public static final String MQTT_CONF_PASSWORD       = "mqtt_password";

    //FIBARO FIELDS
    public static final String ZWAVE                    = "FIBARO";
    public static final String ZWAVE_CONF_HOST          = "zwave_host";
    public static final String ZWAVE_CONF_PORT          = "zwave_port";
    public static final String ZWAVE_CONF_USERNAME      = "zwave_username";
    public static final String ZWAVE_CONF_PASSWORD      = "zwave_password";

    private Activity activity;
    private SharedPreferences sharedPreferences;

    public LuxiomPrefsManager(Activity activity) {
        this.activity = activity;
        initLuxiomPrefMan();
    }

    private void initLuxiomPrefMan(){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.activity);
    }

    public String getStringPreference(String field, String defvalue) {
        if (sharedPreferences != null) {
            try {
                String prefValue = sharedPreferences.getString(field, defvalue);
                return prefValue;
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        } else {
            return "";
        }
    }

    public void saveStringPrefs(HashMap<String, String> prefsMap) {
        if (sharedPreferences != null) {
            for (Map.Entry<String, String> entry : prefsMap.entrySet()) {
                if (entry.getKey() != null && entry.getValue() != null) {
                    sharedPreferences.edit().putString(entry.getKey(), entry.getValue()).apply();
                }
            }
        }
    }
}
