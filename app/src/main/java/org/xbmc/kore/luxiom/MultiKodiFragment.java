package org.xbmc.kore.luxiom;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toolbar;

import org.xbmc.kore.R;
import org.xbmc.kore.jsonrpc.method.Playlist;
import org.xbmc.kore.luxiom.model.KodiDeviceModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by OMIPLEKEVIN on Sep 25, 025.
 */

public class MultiKodiFragment extends Fragment {

    /**
     * for gesture listening
     */
    float x1 = 0F, x2 = 0F, y1 = 0F, y2 = 0F;

    private ListView multiKodiListView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_multikodi, container, false);
        FrameLayout touchOverlayCapture = (FrameLayout) root.findViewById(R.id.touchOverlay);
        multiKodiListView = (ListView) root.findViewById(R.id.multiKodiListing);

        MultiKodiListingAdapter multiKodiListingAdapter = new MultiKodiListingAdapter(getActivity(), createDummyKodiListing());
        multiKodiListView.setAdapter(multiKodiListingAdapter);
        touchOverlayCapture.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    // when user first touches the screen we get x and y coordinate
                    case MotionEvent.ACTION_DOWN: {
                        x1 = event.getX();
                        y1 = event.getY();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        x2 = event.getX();
                        y2 = event.getY();

                        if (y1 < y2) {
                            Log.d(getClass().getSimpleName(), "UP to Down Swap Performed " + y1 + " " + y2);
                        } else if (y1 > y2) {
                            Log.d(getClass().getSimpleName(), "Down to UP Swap Performed " + y1 + " " + y2);
                        }
                        break;
                    }
                }
                return true;
            }
        });
        return root;
    }

    public List<KodiDeviceModel> createDummyKodiListing() {
        List<KodiDeviceModel> listing = new ArrayList<>();
        KodiDeviceModel entry1 = new KodiDeviceModel("My Local Kodi", "192.168.56.1", "PC-Desktop");
        listing.add(entry1);
        KodiDeviceModel entry2 = new KodiDeviceModel("Chrome Cast", "192.168.0.101", "ChromeCast");
        listing.add(entry2);
        KodiDeviceModel entry3 = new KodiDeviceModel("Tablet", "192.168.56.1", "Tablet");
        listing.add(entry3);

        return listing;
    }

    private class MultiKodiListingAdapter extends BaseAdapter {

        Context context;
        List<KodiDeviceModel> kodiDeviceList;

        public MultiKodiListingAdapter(Context context, List<KodiDeviceModel> kodiDeviceList) {
            this.context = context;
            this.kodiDeviceList = kodiDeviceList;
        }

        @Override
        public int getCount() {
            if (kodiDeviceList == null) {
                return 0;
            } else {
                return kodiDeviceList.size();
            }
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = LayoutInflater.from(this.context).inflate(R.layout.item_kodi_list, parent, false);
                viewHolder.deviceThumb = (ImageView) convertView.findViewById(R.id.kodiThumb);
                viewHolder.deviceName = (TextView) convertView.findViewById(R.id.kodiName);
                viewHolder.deviceAddress = (TextView) convertView.findViewById(R.id.kodiAddress);
                viewHolder.deviceType = (TextView) convertView.findViewById(R.id.kodiType);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.deviceName.setText(kodiDeviceList.get(position).kodiName);
            viewHolder.deviceAddress.setText(kodiDeviceList.get(position).kodiAddress);
            viewHolder.deviceType.setText(kodiDeviceList.get(position).kodiType);

            return convertView;
        }

        class ViewHolder {
            ImageView deviceThumb;
            TextView deviceName;
            TextView deviceAddress;
            TextView deviceType;
        }
    }
}
