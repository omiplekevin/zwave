package org.xbmc.kore.luxiom;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.xbmc.kore.R;
import org.xbmc.kore.luxiom.interfaces.NetworkRESTApiAsyncCallback;
import org.xbmc.kore.luxiom.model.RequestModelWrapper;
import org.xbmc.kore.luxiom.network.NetworkRESTApiAsync;

import java.io.File;
import java.util.HashMap;

/**
 *
 * Created by OMIPLEKEVIN on Sep 20, 020.
 */

public class BinaryBeanNetworkConfigFragment extends Fragment implements NetworkRESTApiAsyncCallback{

    private static final int PERMISSIONS_REQUEST_WRITE_READ_EXTERNAL_STORAGE = 100;

    private static final String DIR_KODI = Environment.getExternalStorageDirectory() + "/Kodi";
    private static final String DIR_KODI_JSON = DIR_KODI + "/json";
    private static final String ROOMS_JSON_FILENAME = "rooms.json";
    private static final String DEVICES_JSON_FILENAME = "devices.json";
    private static final String SCENES_JSON_FILENAME = "scenes.json";

    public static final String API_PATH = "/iscientTA/updateCache/";
    public static final String API_DEVICES = "?devices";
    public static final String API_ROOMS = "?rooms";
    public static final String API_SCENES = "?scenes";

    private static final String TAG = "ZwaveNetConf";

    //rpi config
    private EditText rpiConfHost;
    private EditText rpiConfPort;
    private EditText rpiConfUsername;
    private EditText rpiConfPassword;

    //mqtt
    private EditText mqttConfHost;
    private EditText mqttConfPort;
    private EditText mqttConfUsername;
    private EditText mqttConfPassword;

    //fibaro
    private EditText fibaroConfHost;
    private EditText fibaroConfPort;
    private EditText fibaroConfUsername;
    private EditText fibaroConfPassword;

    private LuxiomPrefsManager luxiomPrefsManager;
    private Toast toast;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Activity activity = getActivity();
        View mainView = activity.getLayoutInflater().inflate(R.layout.fragment_zwave_network_config, null);
        luxiomPrefsManager = new LuxiomPrefsManager(getActivity());

        toast = Toast.makeText(getActivity(), null, Toast.LENGTH_LONG);
        String rpiHost = luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.RPI_CONF_HOST,"");
        String rpiPort = luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.RPI_CONF_PORT, "");
        String rpiUsername = luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.RPI_CONF_USERNAME, "");
        String rpiPassword = luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.RPI_CONF_PASSWORD, "");

        String mqttHost = luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.MQTT_CONF_HOST,"");
        String mqttPort = luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.MQTT_CONF_PORT, "");
        String mqttUsername = luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.MQTT_CONF_USERNAME, "");
        String mqttPassword = luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.MQTT_CONF_PASSWORD, "");

        String zwaveHost = luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.ZWAVE_CONF_HOST,"");
        String zwavePort = luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.ZWAVE_CONF_PORT, "3480");
        String zwaveUsername = luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.ZWAVE_CONF_USERNAME, "");
        String zwavePassword = luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.ZWAVE_CONF_PASSWORD, "");

        //rpi
        rpiConfHost = (EditText) mainView.findViewById(R.id.netConfHost);
        if (rpiConfHost != null) {
            rpiConfHost.setText(rpiHost);
        }

        rpiConfPort = (EditText) mainView.findViewById(R.id.netConfPort);
        if (rpiConfPort != null) {
            rpiConfPort.setText(rpiPort);
        }

        rpiConfUsername = (EditText) mainView.findViewById(R.id.netConfUsername);
        if (rpiConfUsername != null) {
            rpiConfUsername.setText(rpiUsername);
        }

        rpiConfPassword = (EditText) mainView.findViewById(R.id.netConfPassword);
        if (rpiConfPassword != null) {
            rpiConfPassword.setText(rpiPassword);
        }

        //mqtt
        mqttConfHost = (EditText) mainView.findViewById(R.id.mqttConfHost);
        if (mqttConfHost != null) {
            mqttConfHost.setText(mqttHost);
        }
        mqttConfPort = (EditText) mainView.findViewById(R.id.mqttConfPort);
        if (mqttConfPort != null) {
            mqttConfPort.setText(mqttPort);
        }
        mqttConfUsername = (EditText) mainView.findViewById(R.id.mqttConfUsername);
        if (mqttConfUsername != null) {
            mqttConfUsername.setText(mqttUsername);
        }
        mqttConfPassword = (EditText) mainView.findViewById(R.id.mqttConfPassword);
        if (mqttConfPassword != null) {
            mqttConfPassword.setText(mqttPassword);
        }

        //fibaro
        fibaroConfHost = (EditText) mainView.findViewById(R.id.fibaroConfHost);
        if (fibaroConfHost != null) {
            fibaroConfHost.setText(zwaveHost);
        }
        fibaroConfPort = (EditText) mainView.findViewById(R.id.fibaroConfPort);
        if (fibaroConfPort != null) {
            fibaroConfPort.setText(zwavePort);
        }
        fibaroConfUsername = (EditText) mainView.findViewById(R.id.fibaroConfUsername);
        if (fibaroConfUsername != null) {
            fibaroConfUsername.setText(zwaveUsername);
        }
        fibaroConfPassword = (EditText) mainView.findViewById(R.id.fibaroConfPassword);
        if (fibaroConfPassword != null) {
            fibaroConfPassword.setText(zwavePassword);
        }

        Button configSaveBtn = (Button) mainView.findViewById(R.id.netConfSaveBtn);
        if (configSaveBtn != null) {
            configSaveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HashMap<String, String> prefValues = new HashMap<>();
                    prefValues.put(LuxiomPrefsManager.RPI_CONF_HOST, rpiConfHost.getText().toString().trim());
                    prefValues.put(LuxiomPrefsManager.RPI_CONF_PORT, rpiConfPort.getText().toString().trim());
                    prefValues.put(LuxiomPrefsManager.RPI_CONF_USERNAME, rpiConfUsername.getText().toString().trim());
                    prefValues.put(LuxiomPrefsManager.RPI_CONF_PASSWORD, rpiConfPassword.getText().toString().trim());

                    prefValues.put(LuxiomPrefsManager.MQTT_CONF_HOST, mqttConfHost.getText().toString().trim());
                    prefValues.put(LuxiomPrefsManager.MQTT_CONF_PORT, mqttConfPort.getText().toString().trim());
                    prefValues.put(LuxiomPrefsManager.MQTT_CONF_USERNAME, mqttConfUsername.getText().toString().trim());
                    prefValues.put(LuxiomPrefsManager.MQTT_CONF_PASSWORD, mqttConfPassword.getText().toString().trim());

                    prefValues.put(LuxiomPrefsManager.ZWAVE_CONF_HOST, fibaroConfHost.getText().toString().trim());
                    prefValues.put(LuxiomPrefsManager.ZWAVE_CONF_PORT, fibaroConfPort.getText().toString().trim());
                    prefValues.put(LuxiomPrefsManager.ZWAVE_CONF_USERNAME, fibaroConfUsername.getText().toString().trim());
                    prefValues.put(LuxiomPrefsManager.ZWAVE_CONF_PASSWORD, fibaroConfPassword.getText().toString().trim());

                    luxiomPrefsManager.saveStringPrefs(prefValues);
                    getActivity().onBackPressed();
                }
            });
        }

        Button updateConfBtn = (Button) mainView.findViewById(R.id.updateJsonBtn);
        if (updateConfBtn != null) {
            updateConfBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkReadWritePermissionAndUpdate();
                }
            });
        }

        return mainView;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    boolean updatedRooms = false;
    boolean updatedDevices = false;
    boolean updatedScenes = false;

    @Override
    public void onNetworkRequestResult(RequestModelWrapper content, String tag) {
        if (content.requestCode == RequestModelWrapper.REQUEST_BAD) {
            //bad catch of error code
            if (content.requestResult == null) {
                content.requestResult = "Failed or Invalid Configuration";
            }
            switch (tag) {
                case "rooms":
                    updatedRooms = false;
                    break;
                case "devices":
                    updatedDevices = false;
                    break;
                case "scenes":
                    updatedScenes = false;
                    break;
            }
            Toast.makeText(getActivity(), content.requestResult, Toast.LENGTH_LONG).show();
            Log.i(TAG, "onNetworkRequestUIUpdateResult: " + content);
        } else {
            switch (tag) {
                case "rooms":
                    updatedRooms = true;
                    break;
                case "devices":
                    updatedDevices = true;
                    break;
                case "scenes":
                    updatedScenes = true;
                    break;
            }
            toast.setText("Updated " + (updatedRooms?"rooms":"") + (updatedDevices?", devices":"") + (updatedScenes?" and scenes":""));
            toast.show();
            Log.i(TAG, "onNetworkRequestUIUpdateResult: " + content);
        }
    }

    private void updateRooms(String sourceUrl) {
        NetworkRESTApiAsync networkRESTApiAsync = new NetworkRESTApiAsync(sourceUrl, true, DIR_KODI_JSON, ROOMS_JSON_FILENAME, "GET", null);
        networkRESTApiAsync.setNetworkRESTApiAsyncCallback(this, "rooms");
        networkRESTApiAsync.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
    }

    private void updateDevices(String sourceUrl) {
        NetworkRESTApiAsync networkRESTApiAsync = new NetworkRESTApiAsync(sourceUrl, true, DIR_KODI_JSON, DEVICES_JSON_FILENAME, "GET", null);
        networkRESTApiAsync.setNetworkRESTApiAsyncCallback(this, "devices");
        networkRESTApiAsync.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
    }

    private void updateScenes(String sourceUrl) {
        NetworkRESTApiAsync networkRESTApiAsync = new NetworkRESTApiAsync(sourceUrl, true, DIR_KODI_JSON, SCENES_JSON_FILENAME, "GET", null);
        networkRESTApiAsync.setNetworkRESTApiAsyncCallback(this, "scenes");
        networkRESTApiAsync.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
    }

    private boolean checkReadWritePermissionAndUpdate(){
        int hasWritePermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        // Here, thisActivity is the current activity
        if ( hasWritePermission != PackageManager.PERMISSION_GRANTED) {

            if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                showMessageOKCancel("You need to allow access to Contacts",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(getActivity(), new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        PERMISSIONS_REQUEST_WRITE_READ_EXTERNAL_STORAGE);
                            }
                        });
                return false;
            }
            ActivityCompat.requestPermissions(getActivity(), new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSIONS_REQUEST_WRITE_READ_EXTERNAL_STORAGE);
            return false;
        }
        if (luxiomPrefsManager == null) {
            Toast.makeText(getActivity(), "Unable to access preferences...", Toast.LENGTH_LONG).show();
        } else {
            String host = luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.RPI_CONF_HOST,"");
            String port = luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.RPI_CONF_PORT, "");
            String username = luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.RPI_CONF_USERNAME, "");
            String password = luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.RPI_CONF_PASSWORD, "");

            updateRooms("http://" + host + ":" + port + API_PATH + API_ROOMS);
            updateDevices("http://" + host + ":" + port + API_PATH + API_DEVICES);
            updateScenes("http://" + host + ":" + port + API_PATH + API_SCENES);
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_WRITE_READ_EXTERNAL_STORAGE: {
                Log.i(TAG, "case: " + PERMISSIONS_REQUEST_WRITE_READ_EXTERNAL_STORAGE);
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "we have now the permission to write/read files in storage");
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Log.i(TAG, "Permission denied.");
                    Toast.makeText(getActivity(), "Permission denied", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    // TODO: Sep 19, 019 PUT TO UTILS LATER
    private void initializeAppFolder(){
        File appFolder = new File(DIR_KODI_JSON);
        if (appFolder.mkdirs()) {
            Log.i(TAG, DIR_KODI_JSON + " created");
        } else {
            Log.w(TAG, DIR_KODI_JSON + " error creating app folder OR folder is already present");
        }
    }
}
