package org.xbmc.kore.luxiom;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xbmc.kore.R;
import org.xbmc.kore.luxiom.interfaces.NetworkRESTApiAsyncCallback;
import org.xbmc.kore.luxiom.model.RequestModelWrapper;
import org.xbmc.kore.luxiom.model.SceneModel;
import org.xbmc.kore.luxiom.network.NetworkRESTApiAsync;
import org.xbmc.kore.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.xbmc.kore.luxiom.Constants.DIR_KODI_JSON;
import static org.xbmc.kore.luxiom.Constants.SCENES_JSON_FILENAME;

/**
 * Created by OMIPLEKEVIN on Sep 24, 024.
 */

public class SceneGridFragment extends Fragment implements MqttCallback, NetworkRESTApiAsyncCallback{

    public static final String ERR_NO_SCENE_JSON = "Please update, no scenes.json";

    /**
     * for gesture listening
     */
    float x1 = 0F, x2 = 0F, y1 = 0F, y2 = 0F;

    GridView sceneGridView;
    List<SceneModel> scenes;

    private static final String CONTROLLER = "/data_request?id=lu_action&serviceId=";
    private static final String SERVICE_ID = "urn:micasaverde-com:serviceId:HomeAutomationGateway1";
    private static final String ACTION = "RunScene";
    private static final String MQTT_TOPIC_TREE_SCENES = "/binarybean/vera/status/scenes";

    private String PORT = "";
    private String HOST = "";

    private LuxiomPrefsManager luxiomPrefsManager;
    private MqttAndroidClient mqttAndroidClient;
    private boolean hasStartedMqtt = false;
    private SceneGridAdapter sceneGridAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        luxiomPrefsManager = new LuxiomPrefsManager(getActivity());
        PORT = luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.ZWAVE_CONF_PORT, "3480");
        HOST = luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.ZWAVE_CONF_HOST, "");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_scene, container, false);
        FrameLayout touchOverlayCapture = (FrameLayout) root.findViewById(R.id.touchOverlay);
        touchOverlayCapture.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    // when user first touches the screen we get x and y coordinate
                    case MotionEvent.ACTION_DOWN: {
                        x1 = event.getX();
                        y1 = event.getY();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        x2 = event.getX();
                        y2 = event.getY();

                        if (y1 < y2) {
                            Log.d(getClass().getSimpleName(), "UP to Down Swap Performed " + y1 + " " + y2);
                            showMultiKodiScreen();
                        } else if (y1 > y2) {
                            Log.d(getClass().getSimpleName(), "Down to UP Swap Performed " + y1 + " " + y2);
                        }
                        break;
                    }
                }
                return true;
            }
        });
        sceneGridView = (GridView) root.findViewById(R.id.sceneGridView);
        Log.e(getClass().getSimpleName(), "HOST: " + HOST);
        return root;
    }

    private void showMultiKodiScreen() {
        getFragmentManager().beginTransaction()
                .replace(R.id.container, new MultiKodiFragment())
                .addToBackStack("multikodi")
                .commit();
    }

    @Override
    public void onResume() {
        Log.e(getClass().getSimpleName(), "onResume()");

        updateScenes();
        if (scenes != null) {
            if (!hasStartedMqtt) {
                hasStartedMqtt = true;
                startMqttService(scenes);
            } else {
                Log.i(getClass().getSimpleName(), "MQTT has already started");
            }
            sceneGridAdapter = new SceneGridAdapter(getActivity().getApplicationContext(), scenes);
            if (scenes.size() > 5) {
                sceneGridView.setNumColumns(5);
            } else {
                sceneGridView.setNumColumns(scenes.size());
            }
            sceneGridView.setAdapter(sceneGridAdapter);
        } else {

            Toast.makeText(getActivity(), "Unable to render scenes, please update cache", Toast.LENGTH_LONG).show();
        }

        super.onResume();
    }

    @Override
    public void onPause() {
        hasStartedMqtt = false;
        try {
            if (scenes != null) {
                for (SceneModel scene : scenes) {
                    unsubscribeMqtt(MQTT_TOPIC_TREE_SCENES + "/" + scene.sceneID);
                }
//                disconnectMqtt();
            }
        } catch (Exception e) {
//            disconnectMqtt();
            e.printStackTrace();
        }
        super.onPause();
    }

    private void startMqttService(final List<SceneModel> devices) {
        BluetoothAdapter myDevice = BluetoothAdapter.getDefaultAdapter();
        String deviceName = "";
        if (myDevice == null) {
            deviceName = Build.MODEL;
        } else {
            deviceName = myDevice.getName();
        }

        deviceName = deviceName.replace("\'", "");
        String clientId = "Luxiom-" + deviceName;
        Log.i(getClass().getSimpleName(),
                "================================================\n" +
                        "deviceName: " + deviceName + ", client: " + clientId +
                        "\n================================================");

        Log.i(getClass().getSimpleName(), "tcp://" + luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.MQTT_CONF_HOST, "")
                + ":" + luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.MQTT_CONF_PORT, ""));

        mqttAndroidClient =
                new MqttAndroidClient(getActivity(),
                        "tcp://" + luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.MQTT_CONF_HOST, "")
                                + ":" + luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.MQTT_CONF_PORT, ""),
                        clientId);

        try {
            mqttAndroidClient.setCallback(SceneGridFragment.this);
            IMqttToken token = mqttAndroidClient.connect();
            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // We are connected
                    Log.d(getClass().getSimpleName(), "onSuccess");
                    for (SceneModel sceneModel : devices) {
                        subscribeMqtt(MQTT_TOPIC_TREE_SCENES + "/" + sceneModel.sceneID);
                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.d(getClass().getSimpleName(), "onFailure " + exception);
                    String err = "Failed connecting to Broker " + luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.MQTT_CONF_HOST,"") + ":" + luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.MQTT_CONF_PORT, "");
                    Toast.makeText(getActivity(), err, Toast.LENGTH_SHORT).show();
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    private void subscribeMqtt(String topic) {
        final int qos = 1;
        try {
            IMqttToken subToken = mqttAndroidClient.subscribe(topic, qos);
            subToken.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // The message was published
                    Log.i(getClass().getSimpleName(), Arrays.toString(asyncActionToken.getTopics()) + " - " + asyncActionToken.getClient().getClientId() + " - " + asyncActionToken.getClient().getServerURI());
                    Log.e(getClass().getSimpleName(), "onSuccess");
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken,
                                      Throwable exception) {
                    String err = "Failed connecting to Broker " + luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.MQTT_CONF_HOST,"") + ":" + luxiomPrefsManager.getStringPreference(LuxiomPrefsManager.MQTT_CONF_PORT, "");
                    Toast.makeText(getActivity(), err, Toast.LENGTH_SHORT).show();
                    Log.e(getClass().getSimpleName(), "onFailure");
                    // The subscription could not be performed, maybe the user was not
                    // authorized to subscribe on the specified topic e.g. using wildcards

                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    private void unsubscribeMqtt(final String topic) {
        if (mqttAndroidClient != null) {
            try {
                IMqttToken unsubToken = mqttAndroidClient.unsubscribe(topic);
                unsubToken.setActionCallback(new IMqttActionListener() {
                    @Override
                    public void onSuccess(IMqttToken asyncActionToken) {
                        Log.i(getClass().getSimpleName(), "onSuccess unsubscribeMqtt on" + topic);
                    }

                    @Override
                    public void onFailure(IMqttToken asyncActionToken,
                                          Throwable exception) {
                        // some error occurred, this is very unlikely as even if the client
                        // did not had a subscription to the topic the unsubscribe action
                        // will be successfully
                    }
                });
            } catch (MqttException e) {
                e.printStackTrace();
            }
        } else {
            Log.w(getClass().getSimpleName(), "mqttAndroidClient is null");
        }
    }

    private void disconnectMqtt() {
        try {
            if (mqttAndroidClient != null) {
                IMqttToken disconToken = mqttAndroidClient.disconnect();
                if (disconToken != null) {
                    disconToken.setActionCallback(new IMqttActionListener() {
                        @Override
                        public void onSuccess(IMqttToken asyncActionToken) {
                            Log.i(getClass().getSimpleName(), "onSuccess disconnectMqtt");
                            // we are now successfully disconnected
                        }

                        @Override
                        public void onFailure(IMqttToken asyncActionToken,
                                              Throwable exception) {
                            // something went wrong, but probably we are disconnected anyway
                        }
                    });
                }
            }
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void connectionLost(Throwable cause) {
        try {
            Log.e(getClass().getSimpleName(), "connection lost - " + cause.getMessage());
            Toast.makeText(getActivity(), cause.getMessage(), Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        //{"name":"Kitchen Off","id":9,"room":5,"active":1}
        try {
            boolean hasFoundScene = false;
            JSONObject jsonObject = new JSONObject(message.toString());
            Log.e("messageArrived", jsonObject.toString());
            for (SceneModel sceneItem : scenes) {
                if (jsonObject.getInt("id") == sceneItem.sceneID) {
                    sceneItem.isActive = (jsonObject.getInt("active") == 1);
                    hasFoundScene = true;
                    sceneGridAdapter.notifyDataSetChanged();
                    break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
    }

    @Override
    public void onAttach(Context context) {
        Log.e(getClass().getSimpleName(), "onAttach()");
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        Log.e(getClass().getSimpleName(), "onDetach()");
        try {
            disconnectMqtt();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDetach();
    }

    private void updateScenes() {
        File roomFiles = new File(DIR_KODI_JSON + "/" + SCENES_JSON_FILENAME);
        if (!roomFiles.isFile()) {
//            getRoomsJSON();
            Log.i(getClass().getSimpleName(), ERR_NO_SCENE_JSON);
        } else {
            if (scenes != null) {
                scenes.clear();
            } else {
                scenes = new ArrayList<>();
            }
            try {
                JSONObject roomsRawJson = new JSONObject(Utils.readCacheFile(DIR_KODI_JSON + "/" + SCENES_JSON_FILENAME));
                Log.i(getClass().getSimpleName(), roomsRawJson.toString());
                JSONArray roomsArray = roomsRawJson.getJSONArray("array");
                for (int i = 0; i < roomsArray.length(); i++) {
                    try {
                        JSONObject obj = roomsArray.getJSONObject(i);
                        SceneModel entry = new SceneModel(obj.getString("Name"), obj.getInt("ID"));
                        scenes.add(entry);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onNetworkRequestResult(RequestModelWrapper content, String tag) {
    }

    private class SceneGridAdapter extends BaseAdapter {

        private Context context;
        private List<SceneModel> scenes;

        public SceneGridAdapter(Context context, List<SceneModel> scenes) {
            this.context = context;
            this.scenes = scenes;
        }

        @Override
        public int getCount() {
            if (scenes != null) {
                return scenes.size();
            } else {
                return 0;
            }
        }

        @Override
        public SceneModel getItem(int position) {
            return scenes.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final int pos = position;
            final ViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = LayoutInflater.from(this.context).inflate(R.layout.item_scene_grid, parent, false);
                viewHolder.button = (ToggleButton) convertView.findViewById(R.id.sceneBtn);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.button.setText(scenes.get(position).sceneName);
            viewHolder.button.setTextOff(scenes.get(position).sceneName);
            viewHolder.button.setTextOn(scenes.get(position).sceneName);
            viewHolder.button.setChecked(scenes.get(position).isActive);
            viewHolder.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = "http://" + HOST + ":" + PORT + CONTROLLER + SERVICE_ID + "&action=" + ACTION + "&SceneNum=" + scenes.get(pos).sceneID + "&output_format=json";
                    Log.e(getClass().getSimpleName(), "clicked, send " + url);
                    NetworkRESTApiAsync networkRESTApiAsync = new NetworkRESTApiAsync(url, false, null, null, "GET", null);
                    networkRESTApiAsync.setNetworkRESTApiAsyncCallback(new NetworkRESTApiAsyncCallback() {
                        @Override
                        public void onNetworkRequestResult(RequestModelWrapper content, String tag) {

                            if (content.requestCode == RequestModelWrapper.REQUEST_BAD) {
                                //bad catch of error code
                                viewHolder.button.setChecked(false);
                                if (content.requestResult == null) {
                                    content.requestResult = "Failed or Invalid Configuration";
                                }
                                Toast.makeText(getActivity(), content.requestResult, Toast.LENGTH_LONG).show();
                                Log.i(getClass().getSimpleName(), "onNetworkRequestUIUpdateResult: " + content);
                            } else {
                                Log.i(getClass().getSimpleName(), "onNetworkRequestUIUpdateResult: " + content);
                                Toast.makeText(context, "Ok", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, "");
                    networkRESTApiAsync.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                }
            });
            return convertView;
        }

        class ViewHolder{

            public ToggleButton button;

        }
    }

}
