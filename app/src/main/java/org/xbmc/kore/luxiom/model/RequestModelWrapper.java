package org.xbmc.kore.luxiom.model;

/**
 * Created by OMIPLEKEVIN on Sep 16, 016.
 */

public class RequestModelWrapper {

    public static final int REQUEST_OK = 1;
    public static final int REQUEST_BAD = -1;

    public int requestCode;
    public String requestResult;

    @Override
    public String toString() {
        return requestCode + " " + requestResult;
    }
}
