package org.xbmc.kore.luxiom.interfaces;

import org.xbmc.kore.luxiom.model.RequestModelWrapper;

/**
 * Created by OMIPLEKEVIN on Sep 17, 017.
 */

public interface NetworkRESTApiAsyncCallback {

    void onNetworkRequestResult(RequestModelWrapper content, String tag);

}
